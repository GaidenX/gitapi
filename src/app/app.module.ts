import { NgModule }         from '@angular/core';
import { BrowserModule }    from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';

// Get Issues
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { IssuesComponent } from './issues/issues.component';
import { ConfigService } from './config-service.service';

// Post Form
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    IssuesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [ConfigService],
  bootstrap: [AppComponent]
})
export class AppModule { }
