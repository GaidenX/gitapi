import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable(
)
export class ConfigService {
  constructor(private http: HttpClient) { }

  private configUrl = 'https://api.github.com/repos/gaidenxx/git-api/issues';

  httpOptions = {
    headers: new HttpHeaders(
      {
        'Authorization' : 'token 641dd0fbf86d620b9840b70c0df27dc73782bb00'
      }
    )
  }

  getConfig() {
   return this.http.get(this.configUrl)
  }

  createIssue(data){
    this.http.post(this.configUrl,data,this.httpOptions).subscribe(
         data => {
             console.log("POST success", data);
         },
         error => {
             console.log("Error", error);
         }
     )
  }
}