import { Component, OnInit, Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { ConfigService } from '../config-service.service';

@Component({
  selector: 'app-issues',
  templateUrl: './issues.component.html',
  styleUrls: ['./issues.component.css']
})
export class IssuesComponent implements OnInit {
  issuesArray = [];
  
  constructor(private gitService : ConfigService){}

  ngOnInit(){
    this.gitService.getConfig().subscribe( data =>
      {
        for (var key in data){
          this.issuesArray.push(data[key]);
        }
      }
    );

    console.log(this.issuesArray);
  }

  // Post Issue Form
  issueInfo = {
    title : "",
    body : "",
    assignee : ""
  }
  
  sendIssue(){
    this.gitService.createIssue(this.issueInfo);
  }

}
